<?php
$page_title = "Группы";
require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/header.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/db/connectionDB.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/groups/model_groups.php";

change_group($_GET['group_ID'],$_GET['name']);
$currentNameGroup = change_group_button($_GET['ID']);

?>
    <div class="container px-4">
        <nav class="navbar navbar-light bg-light">
            <span class="navbar-brand mb-0 h1"><a href="/groups/">Группа</a> / <a href="">Изменение</a></span>
        </nav>

        <form action="" method="get">
            <div class="form-group row">
                <label for="name" class="col-3 col-form-label">Название группы</label>
                <div class="col-5">
                    <input type="text" class="form-control" id="name" name="name" value="<?=$currentNameGroup?>">
                </div>
            </div>
            <div class="offset-3 col-9">
                <button type="submit" class="btn btn-primary" name="group_ID" value="<?=$_GET['ID']?>">Отправить</button>
            </div>
        </form>
    </div>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/footer.php"; ?>
