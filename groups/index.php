<?php

$page_title = "Группы";
require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/header.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/db/connectionDB.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/groups/model_groups.php";

?>
<div class="container px-4">
    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1"><a href="/groups/">Группы</a> / <a href="/groups/add/">Новая запись</a></span>
    </nav>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Группа</th>
            <th scope="col">Действия</th>
        </tr>
        </thead>
        <tbody>
            <?php $arrGroups = main_index();
                foreach ($arrGroups as $key => $value):
            ?>
            <tr>
                <th scope='row'> <?= $value['id'] ?> </th>
                <td><?= $value['name'] ?></td>
                <td>
                    <form action="" method="get">
                        <button type='submit' formaction="/groups/edit/" class='btn btn-primary' name="ID" value="<?=$value['id']?>">Изменить</button>
                        <button type='submit' formaction="/groups/delete/" class='btn btn-danger' name="ID" value="<?=$value['id']?>">Удалить</button>
                    </form>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/footer.php"; ?>
