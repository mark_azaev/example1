<?php

$page_title = "Новая группа";
require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/header.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/db/connectionDB.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/groups/model_groups.php";

new_group($_GET['create'],$_GET['name']);

?>

<div class="container">
    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1"><a href="/groups/">Группы</a> / <a href="/groups/add/">Новая запись</a></span>
    </nav>
    <div class="container px-4">
        <form action="" method="get">
            <div class="form-group row">
                <label for="name" class="col-3 col-form-label">Название группы</label>
                <div class="col-5">
                    <input type="text" class="form-control" id="name" name="name">
                </div>
            </div>
            <div class="offset-3 col-9">
                <button type="submit" class="btn btn-primary" name="create">Отправить</button>
            </div>
        </form>
    </div>
</div>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/footer.php"; ?>