<?php

function new_group($getCreate, $getName){

    if(isset($getCreate) && !empty($getName)){
        global $pdo;
        $getName = htmlspecialchars(trim($getName));
        $stmt = $pdo->prepare("INSERT INTO `Groups`(name) VALUES(:name)");
        $stmt->bindParam(":name",$getName);
        $stmt->execute();

        header('Location: /groups/');
        exit;
    }
}
function delete_group($delete){

    if(isset($delete))
    {
        global $pdo;
        $stmt= $pdo->prepare("DELETE FROM `Groups` WHERE id=:id");
        $stmt->bindParam(":id",$delete);
        $stmt->execute();

        header('Location: /groups/');
        exit;
    }
}
function change_group($change_group,$name){

    if(isset($change_group) && isset($name) && !empty($name))
    {
        global $pdo;
        $stmt=$pdo->prepare("UPDATE `Groups` SET name=:name WHERE id=:id");
        $stmt->bindParam(":id",$change_group);
        $stmt->bindParam(":name",$name);
        $stmt->execute();

        header('Location: /groups/');
        exit;
    }
}
function change_group_button($id){

    if(isset($id)) {
        global $pdo;
        $stmt = $pdo->prepare('SELECT `Groups`.`name` FROM `Groups` WHERE `id`=:id');
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row['name'];
    }

    return [];
}
function main_index() : array{

    global $pdo;
    $groups = array();

    $stmt = $pdo->query('SELECT * FROM `Groups`');
    while ($row = $stmt->fetch()) {
        $group =[
            'id' => $row['id'],
            'name' => $row['name']
        ];

        $groups[] = $group;
    }
    return $groups;
}