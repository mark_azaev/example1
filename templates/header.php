<?php
/**
 * @var string $page_title
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo($page_title);?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/styles/main.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <a class="navbar-brand" href="/">
        <img src="/images/logo.png" alt="logo" style="width:40px;">
    </a>


    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="/allRating/">Рейтинг студентов</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/groups/">Группы</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/students/">Студенты</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/ratings/">Оценки</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/subjects/">Предметы</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/relations/">Свзязь: группы-предметы</a>
        </li>
    </ul>
</nav>