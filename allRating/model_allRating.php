<?php
function select_group($selectGroup) : array{
    global $pdo;
    $arrGroups = array();

    $stmt = $pdo->query('SELECT * FROM `Groups`');
    while ($row = $stmt->fetch()) {
        $group = [
            'id'=>$row['id'],
            'name'=>$row['name']
        ];

        $arrGroups[] = $group;

    }

    for ($i = 0; $i < count($arrGroups); $i++){
        if($arrGroups[$i]['id'] == $selectGroup){

            $temp = $arrGroups[$i];
            $arrGroups[$i] = $arrGroups[0];
            $arrGroups[0] = $temp;

            break;
        }
    }

    return $arrGroups;
}

function show_subjects(&$arr1) : array{
    $arr1 = array();
    $subjects = array();
    global $pdo;

    $sql = "SELECT * FROM Subjects";
    $stmt = $pdo->query($sql);
    $i = 0;
    while ($row = $stmt->fetch()) {

        $arr1[$i]= $row['id'];
        $i++;

        $subject =[
            'id'  => $row['id'],
            'name'=> $row['name']
        ];
        $subjects[] = $subject;

    }

    return $subjects;
}

function show_students_and_ratings($select_group,&$arr1,&$existStudentsOrNot) : array{

    $students_and_ratings=array();

    global $pdo;
    $sql = "SELECT id,first_name,last_name FROM Students WHERE group_id = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([$select_group]);

    $existStudentsOrNot = 0;
    while ($row = $stmt->fetch()) {
        $existStudentsOrNot++;

        $local_student_and_rating = [
            'first_name' => $row['first_name'],
            'last_name' => $row['last_name'],
        ];

        for($j = 0; $j < count($arr1); $j++)
        {
            $stmt1 = $pdo->prepare('SELECT id_subject,id_student,rate FROM Ratings WHERE id_student =:id_student AND id_subject = :id_subject');
            $stmt1->bindParam(":id_student",$row['id']);
            $stmt1->bindParam(":id_subject",$arr1[$j]);
            $stmt1->execute();

            $sum = 0;
            $count = 0;
            while($result = $stmt1->fetch()){
                $sum += $result['rate'];
                $count++;
            }

            if($count > 0) {
                $local_student_and_rating[] = [
                    'sum' => round($sum/(float)$count,2),
                ];
            }else{
                $local_student_and_rating[] = [
                    'sum' => "0",
                ];
            }
        }

        $students_and_ratings[] = $local_student_and_rating;


    }
    return $students_and_ratings;
}

function exist_students($existStudentsOrNot){
    return $existStudentsOrNot;
}


