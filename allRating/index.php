<?php

$page_title = "Рейтинг студентов";
require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/header.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/db/connectionDB.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/allRating/model_allRating.php";
$arr=[]; $existStudentsOrNot=0;
?>

<div class="container w-100">
    <div>
        <nav class="navbar navbar-light bg-light">
            <span class="navbar-brand mb-0 h1"><a href="/allRating/">Рейтинг студентов</a></span>
        </nav>

        <form action="" method="get">
            <div class="form-group row">
                <label for="select_group" class="col-3 col-form-label">Группа</label>
                <div>
                    <select class="form-control" name="ID" id="select_group">
                        <?php $arrGroup = select_group($_GET['ID']);
                        foreach ($arrGroup as $key => $value):
                        ?>
                        <option value="<?=$value['id'];?>"><?=$value['name'];?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div>
                <button type="submit" class="btn btn-primary" >Построить рейтинг</button>
            </div>
        </form>
    </div>

<?php if(isset($_GET['ID'])):?>
    <div class="lineB">
    <table class="table table-bordered" >
        <thead class="thead-dark">
        <tr>
            <th scope="col">Студенты</th>
            <?php $arrGroup = show_subjects($arr);
            foreach ($arrGroup as $key => $value): ?>
                <th scope="col"><?=$value['name']?></th>
            <?php endforeach; ?>

        </tr>
        </thead>
        <tbody>

            <?php
            $arrShowStudentsAndRatings = show_students_and_ratings($_GET['ID'],$arr,$existStudentsOrNot);
            foreach ($arrShowStudentsAndRatings as $key => $value): ?>
            <tr>
                <td><?=$value['first_name']?> <?=$value['last_name']?></td>

                <?php foreach($value as $key1 => $value1): if(is_array($value1)):?>
                <td><?=$value1['sum']?></td>

                <?php endif; endforeach;?>

            </tr>
            <?php endforeach; ?>

        </tbody>
    </table>
    </div>

    <div class="offset-3 col-3">
        <span class="navbar-brand mb-0 h1">
            <?php $bit = exist_students($existStudentsOrNot); if(!$bit):?>
                <p>Студентов данной группе нет!</p>
            <?php endif;?>
        </span>
    </div>
<?php endif;?>
</div>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/footer.php"; ?>
