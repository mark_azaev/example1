<?php
session_start();
$page_title = "Оценки";
require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/header.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/db/connectionDB.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/ratings/model_ratings.php";

change_rating($_GET['ID_rating'],$_GET['rate'],$_GET['select_subject'],$_GET['select_student']);
$currentRate = change_rating_button($_GET['ID']);
$student = select_student_for_rating($_GET['ID']);
$subject = select_subject_for_rating($_GET['ID']);
?>
    <div class="container px-4">
        <nav class="navbar navbar-light bg-light">
            <span class="navbar-brand mb-0 h1"><a href="/ratings/">Оценки</a> / <a href="">Изменение</a></span>
        </nav>

        <form action="" method="get">
            <div class="form-group row">
                <label for="rate" class="col-3 col-form-label">Новая оценка</label>
                <div class="col-5">
                    <input type="text" class="form-control" id="rate" name="rate" value="<?=$currentRate?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="select_student" class="col-3 col-form-label">Студент</label>
                <div class="col-5">
                    <select class="form-control" name="select_student" id="select_student">
                        <option value="<?=$student['id_student'];?>"> <?=$student['first_name']." ".$student['last_name'];?> </option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="select_subject" class="col-3 col-form-label">Предмет</label>
                <div class="col-5">
                    <select class="form-control" name="select_subject" id="select_subject">
                        <option value="<?=$subject['id_subject']; ?>"><?=$subject['name'];?></option>
                    </select>
                </div>
            </div>
            <div class="offset-3 col-9">
                <button type="submit" class="btn btn-primary" name="ID_rating" value="<?=$_GET['ID']?>">Отправить</button>
            </div>
            <div class="offset-3 col-3">
                <span class="navbar-brand mb-0 h1"><?=$_SESSION['err']?></span>
            </div>
        </form>
    </div>

<?php session_destroy(); require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/footer.php"; ?>
