<?php
function main_index() : array{

    global $pdo;
    $sql = "SELECT `Ratings`.id, `Students`.first_name, `Students`.last_name, `Subjects`.name, `Ratings`.rate FROM `Ratings`
            LEFT JOIN `Students` ON id_student = `Students`.id
            LEFT JOIN `Subjects` ON id_subject = `Subjects`.id";
    $stmt = $pdo->query($sql);
    $ratings = array();
    while ($row = $stmt->fetch())
    {
        $rating = [
            'id'=>$row['id'],
            'first_name'=> $row['first_name'],
            'last_name' => $row['last_name'],
            'name' => $row['name'],
            'rate' => $row['rate']
        ];

        $ratings[] = $rating;
    }

    return $ratings;

}

function new_rating($create,$rate, $id_subject, $id_students){
    if(isset($create) && !empty($rate)){

        $rate = htmlspecialchars(trim($rate));
        $rate = (int)$rate;

        try {
            if ($rate == null) {
                throw new Exception;
            } else if (is_numeric($rate) && ($rate <= 5) && ($rate >= 2)) {
                global $pdo;
                $stmt = $pdo->prepare("INSERT INTO Ratings(rate,id_subject,id_student) 
                                        VALUES(:rate,:id_subject,:id_student)");
                $stmt->bindParam(":rate", $_GET['rate']);
                $stmt->bindParam(":id_subject", $id_subject);
                $stmt->bindParam(":id_student", $id_students);
                $stmt->execute();

                session_destroy();
                header('Location: /ratings/');
                exit;

            }else{
                throw new Exception ;
            }
        }catch (Exception $exp){
            $_SESSION['err'] = "Неверное значение";
            header('Location: '.$_SERVER['HTTP_REFERER']);
            exit;
        }
    }
}

function delete_rating($id){

    if(isset($id))
    {
        global $pdo;
        $stmt=$pdo->prepare("DELETE FROM Ratings WHERE id=:id");
        $stmt->bindParam(":id",$id);
        $stmt->execute();

        header('Location: /ratings/');
        exit;
    }
}

function select_student(): array
{
    global $pdo;
    $students = array();

    $stmt = $pdo->query('SELECT * FROM Students');
    while ($row = $stmt->fetch())
    {
        $student = [
            'id' => $row['id'],
            'first_name' => $row['first_name'],
            'last_name' => $row['last_name']
        ];
        $students[] = $student;

    }
    return $students;
}
function select_subject(): array
{
    global $pdo;
    $subjects = array();

    $stmt = $pdo->query('SELECT * FROM Subjects');
    while ($row = $stmt->fetch())
    {
        $subject = [
            'id' => $row['id'],
            'name' => $row['name']
        ];

        $subjects[] = $subject;

    }
    return $subjects;
}


function change_rating($change_rating,$rate,$select_subject,$select_student){

    if(isset($change_rating) && !empty($rate))
    {
        try {
            $rate = htmlspecialchars(trim($rate));
            $rate = (int)$rate;

            if($rate == null){
                throw new Exception ;
            }else if(is_numeric($rate) && ($rate <= 5) && ($rate >= 2)){
                global $pdo;
                $sql = "UPDATE Ratings 
                        SET rate=:rate, id_subject =:id_subject, id_student =  :id_student 
                        WHERE id=:id";
                $stmt=$pdo->prepare($sql);
                $stmt->bindParam(":id",$change_rating);
                $stmt->bindParam(":rate",$rate);
                $stmt->bindParam(":id_subject",$select_subject);
                $stmt->bindParam(":id_student",$select_student);
                $stmt->execute();
                session_destroy();
                header('Location: /ratings/');
                exit;
            }else{
                throw new Exception ;
            }
        }catch (Exception  $exc){
            $_SESSION['err'] = "Неверное значение";
            header('Location: '.$_SERVER['HTTP_REFERER']);
            exit;
        }


    }
}

function change_rating_button($id){

    if(isset($id)) {
        global $pdo;
        $sql = 'SELECT `Ratings`.`rate` FROM `Ratings` 
                WHERE `Ratings`.`id` = :id';

        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $row = $stmt->fetch();
        return $row['rate'];

    }
    return [];
}


function select_student_for_rating($change) : array{

    global $pdo;

    $stmt = $pdo->prepare('SELECT id_student,first_name,last_name  FROM Ratings 
                                INNER JOIN Students ON Ratings.id_student = Students.id
                                WHERE Ratings.id = ?
                                LIMIT 1');
    $stmt->execute([$change]);
    $row = $stmt->fetch();

    return [
        'id_student' => $row['id_student'],
        'first_name' => $row['first_name'],
        'last_name' => $row['last_name']
    ];
}

function select_subject_for_rating($change): array{
    global $pdo;

    $stmt = $pdo->prepare('SELECT id_subject, Subjects.name FROM Ratings 
                                INNER JOIN Subjects ON Ratings.id_subject = Subjects.id
                                WHERE Ratings.id = ?
                                LIMIT 1');
    $stmt->execute([$change]);
    $row = $stmt->fetch();

    return [
        'id_subject' => $row['id_subject'],
        'name' => $row['name']
    ];
}