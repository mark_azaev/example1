<?php
session_start();
$page_title = "Новая оценка";
require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/header.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/db/connectionDB.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/ratings/model_ratings.php";

new_rating($_GET['create'],$_GET['rate'],$_GET['select_subject'],$_GET['select_student']);

?>
<div class="container px-4">
    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1"><a href="/ratings/">Оценки</a> / <a href="">Новая запись</a></span>
    </nav>

    <form action="" method="get">
        <div class="form-group row">
            <label for="rate" class="col-3 col-form-label">Новая оценка</label>
            <div class="col-5">
                <input type="text" class="form-control" id="rate" name="rate">
            </div>
        </div>
        <div class="form-group row">
            <label for="select_student" class="col-3 col-form-label">Студент</label>
            <div class="col-5">
                <select class="form-control" name="select_student" id="select_student">

                    <?php $arrStudents = select_student();

                    foreach ($arrStudents as $key => $value):?>

                    <option value="<?=$value['id']?>"><?=$value['first_name']." ".$value['last_name']?></option>;

                    <?php endforeach; ?>


                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="select_subject" class="col-3 col-form-label">Предмет</label>
            <div class="col-5">
                <select class="form-control" name="select_subject" id="select_subject">

                    <?php $arrSubjects = select_subject();

                    foreach ($arrSubjects as $key => $value):?>

                        <option value="<?=$value['id']?>"><?=$value['name']?></option>;

                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="offset-3 col-9">
            <button type="submit" class="btn btn-primary" name="create">Отправить</button>
        </div>
    </form>
    <div class="offset-3 col-3">
        <span class="navbar-brand mb-0 h1"><?=$_SESSION['err']?></span>
    </div>
</div>
<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/footer.php"; ?>
