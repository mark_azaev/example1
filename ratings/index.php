<?php

$page_title = "Оценки";
require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/header.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/db/connectionDB.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/ratings/model_ratings.php";

?>
<div class="container px-4">
    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1"><a href="/ratings/">Оценки</a>/ <a href="/ratings/add/">Новая запись</a></span>
    </nav>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">id</th>
            <th scope="col">Оценка</th>
            <th scope="col">Предмет</th>
            <th scope="col">Студент</th>
            <th scope="col">Действия</th>
        </tr>
        </thead>
        <tbody>
        <?php $arrRatings = main_index();
        foreach ($arrRatings as $key => $value):
            ?>
            <tr>
                <th scope='row'> <?= $value['id'] ?> </th>
                <td><?= $value['rate']?></td>
                <td><?= $value['name']?></td>
                <td><?= $value['last_name']." ".$value['first_name'] ?></td>
                <td>
                    <form action="" method="get">
                        <button type='submit' formaction="/ratings/edit/" class='btn btn-primary' name="ID" value="<?=$value['id']?>">Изменить</button>
                        <button type='submit' formaction="/ratings/delete/" class='btn btn-danger' name="ID" value="<?=$value['id']?>">Удалить</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/footer.php"; ?>