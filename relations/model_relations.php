<?php

function show_relations() : array{
    global $pdo;

    $sql = 'SELECT `Groups`.name AS group_name, `Subjects`.name AS subject_name FROM `Groups` 
                    JOIN `Students` ON `Groups`.id = `Students`.group_id
                    JOIN `Ratings`ON `Students`.id = `Ratings`.id_student
                    JOIN `Subjects` ON `Subjects`.id = `Ratings`.id_subject
                    GROUP BY group_name,subject_name';

    $stmt = $pdo->query($sql);
    $curArr = array();
    $groups = array();
    $curArr[0] = "";
    $i = 1;
    while ($row = $stmt->fetch())
    {

        $curArr[$i] = $row['group_name'];

        if($row['group_name'] == $curArr[$i-1]){
            $group = [
                'group_name'=>'',
                'subject_name'=> $row['subject_name']
            ];
        }else{
            $group = [
                'group_name'=>$row['group_name'],
                'subject_name'=> $row['subject_name']
            ];
        }

        $groups[] = $group;
        $i++;
    }
    return $groups;
}