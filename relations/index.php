<?php

$page_title = "Связь: группы-предметы";
require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/header.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/db/connectionDB.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/relations/model_relations.php";
?>


<div class="container px-4">
    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1"><a href="/relations/">Связи</a></span>
    </nav>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Группа</th>
            <th scope="col">Предметы</th>
        </tr>
        </thead>
        <tbody>

            <?php $arrRelations = show_relations();
            foreach ($arrRelations as $key => $value):
            ?>
            <tr>
                <th scope='row'><?=$value['group_name']?></th>
                <td><?=$value['subject_name']?></td>
            </tr>
            <?php endforeach;?>

        </tbody>
    </table>
</div>
<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/footer.php"; ?>