<?php

function new_subject($getCreate, $getName){

    if(isset($getCreate) && !empty($getName)){
        global $pdo;
        $getName = htmlspecialchars(trim($getName));
        $stmt = $pdo->prepare("INSERT INTO `Subjects`(name) VALUES(:name)");
        $stmt->bindParam(":name",$getName);
        $stmt->execute();

        header('Location: /subjects/');
        exit;
    }
}
function delete_subject($delete){

    if(isset($delete))
    {
        global $pdo;
        $stmt= $pdo->prepare("DELETE FROM `Subjects` WHERE id=:id");
        $stmt->bindParam(":id",$delete);
        $stmt->execute();

        header('Location: /subjects/');
        exit;
    }
}
function change_subject($change_subject,$name){

    if(isset($change_subject) && isset($name) && !empty($name))
    {
        global $pdo;
        $stmt=$pdo->prepare("UPDATE `Subjects` SET name=:name WHERE id=:id");
        $stmt->bindParam(":id",$change_subject);
        $stmt->bindParam(":name",$name);
        $stmt->execute();

        header('Location: /subjects/');
        exit;
    }
}
function change_subject_button($id){

    if(isset($id)) {
        global $pdo;
        $stmt = $pdo->prepare('SELECT `Subjects`.`name` FROM `Subjects` WHERE `id`=:id');
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row['name'];
    }
    return [];
}
function main_index() : array{

    global $pdo;
    $subjects = array();

    $stmt = $pdo->query('SELECT * FROM `Subjects`');
    while ($row = $stmt->fetch()) {
        $subject =[
            'id' => $row['id'],
            'name' => $row['name']
        ];

        $subjects[] = $subject;
    }
    return $subjects;
}
