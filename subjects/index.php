<?php

$page_title = "Предметы";
require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/header.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/db/connectionDB.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/subjects/model_subjects.php";

?>
<div class="container px-4">
    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1"><a href="/subjects/"> Предметы </a> / <a href="/subjects/add/">Новая запись</a></span>
    </nav>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">id</th>
            <th scope="col">Название</th>
            <th scope="col">Действие</th>
        </tr>
        </thead>
        <tbody>
        <?php $arrSubjects = main_index();
        foreach ($arrSubjects as $key => $value):
            ?>
            <tr>
                <th scope='row'> <?= $value['id'] ?> </th>
                <td><?= $value['name'] ?></td>
                <td>
                    <form action="" method="get">
                        <button type='submit' formaction="/subjects/edit/" class='btn btn-primary' name="ID" value="<?=$value['id']?>">Изменить</button>
                        <button type='submit' formaction="/subjects/delete/" class='btn btn-danger' name="ID" value="<?=$value['id']?>">Удалить</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/footer.php"; ?>
