<?php

$page_title = "Студенты";
require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/header.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/db/connectionDB.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/students/model_students.php";
?>
<div class="container px-4">
    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1"><a href="/students/">Студенты</a> / <a href="/students/add/">Новая запись</a></span>
    </nav>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Фамилия имя</th>
            <th scope="col">Группа</th>
            <th scope="col">Действия</th>
        </tr>
        </thead>
        <tbody>
        <?php $arrStudents = main_index();
        foreach ($arrStudents as $key => $value):
            ?>
            <tr>
                <th scope='row'> <?= $value['id'] ?> </th>
                <td><?= $value['last_name']." ".$value['first_name'] ?></td>
                <td><?= $value['name'] ?></td>
                <td>
                    <form action="" method="get">
                        <button type='submit' formaction="/students/edit/" class='btn btn-primary' name="ID" value="<?=$value['id']?>">Изменить</button>
                        <button type='submit' formaction="/students/delete/" class='btn btn-danger' name="ID" value="<?=$value['id']?>">Удалить</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/footer.php"; ?>