<?php
function main_index() : array{

    global $pdo;

    $students = array();
    $sql = 'SELECT `Students`.id,`Students`.first_name,`Students`.last_name,`Groups`.name 
                                                FROM `Students` 
                                                INNER JOIN `Groups` 
                                                ON `Students`.group_id = `Groups`.id';
    $stmt = $pdo->query($sql);
    while ($row = $stmt->fetch()) {
        $student = [
            'id'=>$row['id'],
            'first_name'=> $row['first_name'],
            'last_name' => $row['last_name'],
            'name' => $row['name']

        ];
        $students[] = $student;
    }

    return $students;
}
function new_student($getCreate,$getFirstName,$getLastName,$getGroupId){

    if(isset($getCreate) && !empty($getFirstName) && !empty($getLastName) && !empty($getGroupId)){
        global $pdo;
        $stmt=$pdo->prepare("INSERT INTO Students(first_name,last_name,group_id) VALUES(:first_name,:last_name,:group_id)");
        $stmt->bindParam(":first_name",$getFirstName);
        $stmt->bindParam(":last_name",$getLastName);
        $stmt->bindParam(":group_id",$getGroupId);
        $stmt->execute();

        header('Location: /students/');
        exit;
    }
}
function select_group(): array
{
    global $pdo;
    $groups = array();

    $stmt = $pdo->query('SELECT * FROM `Groups`');
    while ($row = $stmt->fetch())
    {
        $group = [
            'id' => $row['id'],
            'name' => $row['name']
        ];

        $groups[] = $group;

    }
    return $groups;
}
function delete_student($id){

    if(isset($id))
    {
        global $pdo;
        $stmt=$pdo->prepare("DELETE FROM Students WHERE id=:id");
        $stmt->bindParam(":id",$id);
        $stmt->execute();

        header('Location: /students/');
        exit;
    }
}

function change_student($id,$first_name,$last_name,$group_id){

    if(isset($id)
        && isset($first_name)
        && isset($last_name)
        && isset($group_id))
    {
        global $pdo;
        $stmt=$pdo->prepare("UPDATE Students SET first_name=:first_name,last_name=:last_name,group_id=:group_id  WHERE id=:id");
        $stmt->bindParam(":id",$id);
        $stmt->bindParam(":first_name",$first_name);
        $stmt->bindParam(":last_name",$last_name);
        $stmt->bindParam(":group_id",$group_id);
        $stmt->execute();

        header('Location: /students/');
        exit;
    }
}

function change_student_button ($id) : array{
    if(isset($id))
    {
        global $pdo;
        $sql = 'SELECT `Students`.`first_name`, `Students`.`last_name`, `Groups`.`id` AS `currentGroupID` FROM `Students` 
                     JOIN `Groups` ON `Groups`.`id` = `Students`.`group_id`
                    WHERE `Students`.`id` = :id';

        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $row = $stmt->fetch();


        return [
            'currentFirstNameStudent' => $row['first_name'],
            'currentLastNameStudent' => $row['last_name'],
            'currentGroupID' => $row['currentGroupID']
        ];
    }
    return [];

}

function select_group_for_change($currentGroupID) : array{

    global $pdo;
    $groups = array();
    $stmt = $pdo->query('SELECT * FROM `Groups`');
    while ($row = $stmt->fetch())
    {
        $group =[
            'id'=> $row['id'],
            'name' => $row['name']
        ];

        $groups[] = $group;
    }

    for ($i = 0; $i < count($groups); $i++){
        if($groups[$i]['id'] == $currentGroupID){

            $temp = $groups[$i];
            $groups[$i] = $groups[0];
            $groups[0] = $temp;

            break;
        }
    }


    return $groups;
}