<?php
$page_title = "Новый студент";
require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/header.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/db/connectionDB.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/students/model_students.php";

new_student($_GET['create'],$_GET['first_name'],$_GET['last_name'],$_GET['group_id']);

?>
<div class="container px-4">
    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1"><a href="/students/">Студенты</a> / <a href="">Новая запись</a></span>
    </nav>

    <form action="" method="get">
        <div class="form-group row">
            <label for="first_name" class="col-3 col-form-label">Новое имя</label>
            <div class="col-5">
                <input type="text" class="form-control" id="first_name" name="first_name">
            </div>
        </div>
        <div class="form-group row">
            <label for="last_name" class="col-3 col-form-label">Новая фамилия</label>
            <div class="col-5">
                <input type="text" class="form-control" id="last_name" name="last_name">
            </div>
        </div>
        <div class="form-group row">
            <label for="group_id" class="col-3 col-form-label">Группа</label>
            <div class="col-5">
                <select class="form-control" name="group_id" id="group_id">

                    <?php $arrGroups = select_group();

                    foreach ($arrGroups as $key => $value):?>

                    <option value="<?=$value['id']?>"><?=$value['name']?></option>;

                    <?php endforeach; ?>


                </select>
            </div>
        </div>
        <div class="offset-3 col-9">
            <button type="submit" class="btn btn-primary" name="create">Отправить</button>
        </div>
    </form>
</div>
<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/templates/footer.php"; ?>
